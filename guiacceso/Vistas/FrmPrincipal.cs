﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using guiacceso.Clases;

namespace guiacceso
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
            inHabilitaBotones();         
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            CerrarVentanas();
        }
        private void CerrarVentanas()
        {
            try
            {
                if (this.MdiChildren.Length > 0)
                {
                    if (ActiveMdiChild != null)
                        ActiveMdiChild.Close();
                }
                else
                {
                    this.Close();
                }
                    
            }
            catch (Exception ex)
            {
                Funciones.msgError(ex.Message);
            }
        }
        private void btnIniciar_Click(object sender, EventArgs e)
        {
            if (Clases.Funciones.f.EstaConectado)
            {
                if (Clases.Funciones.msgResult("Seguro que desea desconectarse del sistema?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, "") == DialogResult.OK)
                {
                    Clases.Funciones.f.DesconectaBD();
                    inHabilitaBotones();
                    btnIniciar.Image = Funciones.Imagen_boton16X16(Funciones.EnumImagenes16X16.Conectar);
                    btnIniciar.Text = "Conectar";
                    sbtnIniciar.Text = "Conectar";
                    foreach (Form c in this.MdiChildren)
                    {
                        c.Close();
                    }
                    this.IsMdiContainer = false;
                }
            }
            else
            {
                Form frm = new Vistas.FrmLogin();
                frm.ShowDialog();
                if (Clases.Funciones.f.EstaConectado)
                {
                    HabilitaBotones();
                    btnIniciar.Image = Funciones.Imagen_boton16X16(Funciones.EnumImagenes16X16.Desconectar);
                    btnIniciar.Text = "Desconectar";
                    sbtnIniciar.Text = "Desconectar";
                    Funciones.formPrinci = this;
                }
            }
        }

        private void HabilitaBotones()
        {
            menuGuias.Enabled = true;
            menuCumplidos.Enabled = true;
            btnRegGuia.Enabled = true;
            btnConsultaGuia.Enabled = true;
            btnCumplidos.Enabled = true;
        }
        private void inHabilitaBotones()
        {
            menuGuias.Enabled = false;
            menuCumplidos.Enabled = false;
            btnRegGuia.Enabled = false;
            btnConsultaGuia.Enabled = false;
            btnCumplidos.Enabled = false;
        }

        private void btnRegGuia_Click(object sender, EventArgs e)
        {
            foreach (Form form in Application.OpenForms)
            {
                if (form.GetType() == typeof(Vistas.FrmAggGuia))
                {
                    form.Activate();
                    return;
                }
            }
            this.IsMdiContainer = true;
            Vistas.FrmAggGuia frm = new Vistas.FrmAggGuia();
            frm.MdiParent = this;
            frm.Show();
            frm.Width = this.Width - 20;
            frm.Height = this.Height - 130;
            frm.Location = new Point(0,0);
            ValidaMdiParent();
        }
        private void ValidaMdiParent()
        {
            try
            {
                if(this.MdiChildren.Length>0)
                {
                    this.IsMdiContainer = true;
                }else
                {
                    this.IsMdiContainer = false;
                }
            }
            catch (Exception ex)
            {
                Funciones.msgError(ex.Message);
            }
        }

        private void btnConsultaGuia_Click(object sender, EventArgs e)
        {
            foreach (Form form in Application.OpenForms)
            {
                if (form.GetType() == typeof(Vistas.FrmConsultaGuia))
                {
                    form.Activate();
                    return;
                }
            }
            this.IsMdiContainer = true;
            Vistas.FrmConsultaGuia frm = new Vistas.FrmConsultaGuia();
            frm.MdiParent = this;
            frm.Show();
            frm.Width = this.Width - 20;
            frm.Height = this.Height - 130;
            frm.Location = new Point(0, 0);
            ValidaMdiParent();
        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {
            btnIniciar.Image = Funciones.Imagen_boton16X16(Funciones.EnumImagenes16X16.Conectar);
            btnRegGuia.Image = Funciones.Imagen_boton16X16(Funciones.EnumImagenes16X16.AgregarGuia);
            btnConsultaGuia.Image = Funciones.Imagen_boton16X16(Funciones.EnumImagenes16X16.BuscarGuia);
            btnSalir.Image = Funciones.Imagen_boton16X16(Funciones.EnumImagenes16X16.Salir);
            btnCumplidos.Image = Funciones.Imagen_boton16X16(Funciones.EnumImagenes16X16.Cumplidos);
            Funciones.ObtenerListaDeServidores();
        }

        private void FrmPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if(Funciones.msgResult("Seguro que desea salir del sistema?",MessageBoxButtons.OKCancel,MessageBoxIcon.Question)==DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                Funciones.msgError(ex.Message);
            }
        }

        private void btnAcercaDe_Click(object sender, EventArgs e)
        {
            Form f = new Vistas.FrmAcercade();
            f.ShowDialog();
        }

        private void btnRegCumplido_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (Form form in Application.OpenForms)
                {
                    if (form.GetType() == typeof(Vistas.FrmCumplidos))
                    {
                        form.Activate();
                        return;
                    }
                }
                this.IsMdiContainer = true;
                Vistas.FrmCumplidos frm = new Vistas.FrmCumplidos();
                frm.MdiParent = this;
                frm.Show();
                frm.Width = this.Width - 20;
                frm.Height = this.Height - 130;
                frm.Location = new Point(0, 0);
                ValidaMdiParent();
            }
            catch (Exception ex)
            {
                Funciones.msgError(ex.Message);
            }
           
        }

        private void btnConsultaCump_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (Form form in Application.OpenForms)
                {
                    if (form.GetType() == typeof(Vistas.FrmTotalizaCumplidos))
                    {
                        form.Activate();
                        return;
                    }
                }
                this.IsMdiContainer = true;
                Vistas.FrmTotalizaCumplidos frm = new Vistas.FrmTotalizaCumplidos();
                frm.MdiParent = this;
                frm.Show();
                frm.Width = this.Width - 20;
                frm.Height = this.Height - 130;
                frm.Location = new Point(0, 0);
                ValidaMdiParent();
            }
            catch (Exception ex)
            {
                Funciones.msgError(ex.Message);
            }
        }
    }
}
