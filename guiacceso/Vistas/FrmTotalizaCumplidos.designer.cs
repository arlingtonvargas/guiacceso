﻿namespace guiacceso.Vistas
{
    partial class FrmTotalizaCumplidos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTotalizaCumplidos));
            this.gbxFiltro = new System.Windows.Forms.GroupBox();
            this.btnTotaliza = new System.Windows.Forms.Button();
            this.dtpFecHasta = new System.Windows.Forms.DateTimePicker();
            this.lblFecHasta = new System.Windows.Forms.Label();
            this.dtpFecDesde = new System.Windows.Forms.DateTimePicker();
            this.lblFecDesde = new System.Windows.Forms.Label();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.gvCumplidos = new System.Windows.Forms.DataGridView();
            this.Sec = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaReg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CantGuias = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UsuReg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbxFiltro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvCumplidos)).BeginInit();
            this.SuspendLayout();
            // 
            // gbxFiltro
            // 
            this.gbxFiltro.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxFiltro.Controls.Add(this.btnTotaliza);
            this.gbxFiltro.Controls.Add(this.dtpFecHasta);
            this.gbxFiltro.Controls.Add(this.lblFecHasta);
            this.gbxFiltro.Controls.Add(this.dtpFecDesde);
            this.gbxFiltro.Controls.Add(this.lblFecDesde);
            this.gbxFiltro.Controls.Add(this.btnSalir);
            this.gbxFiltro.Controls.Add(this.btnConsultar);
            this.gbxFiltro.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxFiltro.Location = new System.Drawing.Point(12, 16);
            this.gbxFiltro.Name = "gbxFiltro";
            this.gbxFiltro.Size = new System.Drawing.Size(825, 53);
            this.gbxFiltro.TabIndex = 23;
            this.gbxFiltro.TabStop = false;
            this.gbxFiltro.Text = "Filtro por fecha";
            // 
            // btnTotaliza
            // 
            this.btnTotaliza.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTotaliza.Image = ((System.Drawing.Image)(resources.GetObject("btnTotaliza.Image")));
            this.btnTotaliza.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTotaliza.Location = new System.Drawing.Point(446, 19);
            this.btnTotaliza.Name = "btnTotaliza";
            this.btnTotaliza.Size = new System.Drawing.Size(93, 25);
            this.btnTotaliza.TabIndex = 22;
            this.btnTotaliza.Text = "Totalizar";
            this.btnTotaliza.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnTotaliza.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnTotaliza.UseVisualStyleBackColor = true;
            this.btnTotaliza.Click += new System.EventHandler(this.btnTotaliza_Click);
            // 
            // dtpFecHasta
            // 
            this.dtpFecHasta.CustomFormat = "dd/MM/yyyy hh:mm tt";
            this.dtpFecHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecHasta.Location = new System.Drawing.Point(251, 19);
            this.dtpFecHasta.Name = "dtpFecHasta";
            this.dtpFecHasta.Size = new System.Drawing.Size(90, 23);
            this.dtpFecHasta.TabIndex = 20;
            // 
            // lblFecHasta
            // 
            this.lblFecHasta.Location = new System.Drawing.Point(180, 22);
            this.lblFecHasta.Name = "lblFecHasta";
            this.lblFecHasta.Size = new System.Drawing.Size(69, 17);
            this.lblFecHasta.TabIndex = 21;
            this.lblFecHasta.Text = "Hasta : ";
            this.lblFecHasta.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpFecDesde
            // 
            this.dtpFecDesde.CustomFormat = "dd/MM/yyyy hh:mm tt";
            this.dtpFecDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecDesde.Location = new System.Drawing.Point(79, 19);
            this.dtpFecDesde.Name = "dtpFecDesde";
            this.dtpFecDesde.Size = new System.Drawing.Size(90, 23);
            this.dtpFecDesde.TabIndex = 13;
            // 
            // lblFecDesde
            // 
            this.lblFecDesde.Location = new System.Drawing.Point(8, 22);
            this.lblFecDesde.Name = "lblFecDesde";
            this.lblFecDesde.Size = new System.Drawing.Size(69, 17);
            this.lblFecDesde.TabIndex = 14;
            this.lblFecDesde.Text = "Desde : ";
            this.lblFecDesde.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalir.Location = new System.Drawing.Point(545, 19);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(77, 25);
            this.btnSalir.TabIndex = 19;
            this.btnSalir.Text = "Salir";
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnConsultar
            // 
            this.btnConsultar.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultar.Image = ((System.Drawing.Image)(resources.GetObject("btnConsultar.Image")));
            this.btnConsultar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConsultar.Location = new System.Drawing.Point(347, 19);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(93, 25);
            this.btnConsultar.TabIndex = 18;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnConsultar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // gvCumplidos
            // 
            this.gvCumplidos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvCumplidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvCumplidos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Sec,
            this.FechaReg,
            this.CantGuias,
            this.UsuReg});
            this.gvCumplidos.Location = new System.Drawing.Point(12, 72);
            this.gvCumplidos.Name = "gvCumplidos";
            this.gvCumplidos.Size = new System.Drawing.Size(825, 381);
            this.gvCumplidos.TabIndex = 22;
            // 
            // Sec
            // 
            this.Sec.HeaderText = "Sec";
            this.Sec.Name = "Sec";
            this.Sec.ReadOnly = true;
            this.Sec.Visible = false;
            // 
            // FechaReg
            // 
            this.FechaReg.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FechaReg.HeaderText = "Fecha";
            this.FechaReg.Name = "FechaReg";
            this.FechaReg.ReadOnly = true;
            // 
            // CantGuias
            // 
            this.CantGuias.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CantGuias.HeaderText = "Cantidad Guias";
            this.CantGuias.Name = "CantGuias";
            this.CantGuias.ReadOnly = true;
            // 
            // UsuReg
            // 
            this.UsuReg.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.UsuReg.HeaderText = "Usuario Reg";
            this.UsuReg.Name = "UsuReg";
            this.UsuReg.ReadOnly = true;
            // 
            // FrmTotalizaCumplidos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 468);
            this.Controls.Add(this.gbxFiltro);
            this.Controls.Add(this.gvCumplidos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmTotalizaCumplidos";
            this.Text = "Consulta Cumplidos";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmTotalizaCumplidos_FormClosed);
            this.gbxFiltro.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvCumplidos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxFiltro;
        private System.Windows.Forms.DateTimePicker dtpFecDesde;
        private System.Windows.Forms.Label lblFecDesde;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.DataGridView gvCumplidos;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sec;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaReg;
        private System.Windows.Forms.DataGridViewTextBoxColumn CantGuias;
        private System.Windows.Forms.DataGridViewTextBoxColumn UsuReg;
        private System.Windows.Forms.Button btnTotaliza;
        private System.Windows.Forms.DateTimePicker dtpFecHasta;
        private System.Windows.Forms.Label lblFecHasta;
    }
}