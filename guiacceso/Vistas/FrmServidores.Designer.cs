﻿namespace guiacceso.Vistas
{
    partial class FrmServidores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmServidores));
            this.btnEliminaServ = new System.Windows.Forms.Button();
            this.btnAddServ = new System.Windows.Forms.Button();
            this.txtServ = new System.Windows.Forms.TextBox();
            this.lbxServ = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btnEliminaServ
            // 
            this.btnEliminaServ.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminaServ.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminaServ.Image")));
            this.btnEliminaServ.Location = new System.Drawing.Point(360, 11);
            this.btnEliminaServ.Name = "btnEliminaServ";
            this.btnEliminaServ.Size = new System.Drawing.Size(34, 25);
            this.btnEliminaServ.TabIndex = 22;
            this.btnEliminaServ.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEliminaServ.UseVisualStyleBackColor = true;
            this.btnEliminaServ.Click += new System.EventHandler(this.btnEliminaServ_Click);
            // 
            // btnAddServ
            // 
            this.btnAddServ.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddServ.Image = ((System.Drawing.Image)(resources.GetObject("btnAddServ.Image")));
            this.btnAddServ.Location = new System.Drawing.Point(327, 11);
            this.btnAddServ.Name = "btnAddServ";
            this.btnAddServ.Size = new System.Drawing.Size(34, 25);
            this.btnAddServ.TabIndex = 21;
            this.btnAddServ.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAddServ.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAddServ.UseVisualStyleBackColor = true;
            this.btnAddServ.Click += new System.EventHandler(this.btnAddServ_Click);
            // 
            // txtServ
            // 
            this.txtServ.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtServ.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtServ.Location = new System.Drawing.Point(12, 11);
            this.txtServ.Name = "txtServ";
            this.txtServ.Size = new System.Drawing.Size(315, 25);
            this.txtServ.TabIndex = 20;
            // 
            // lbxServ
            // 
            this.lbxServ.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxServ.FormattingEnabled = true;
            this.lbxServ.ItemHeight = 17;
            this.lbxServ.Location = new System.Drawing.Point(12, 42);
            this.lbxServ.Name = "lbxServ";
            this.lbxServ.Size = new System.Drawing.Size(382, 225);
            this.lbxServ.TabIndex = 24;
            this.lbxServ.Click += new System.EventHandler(this.lbxServ_Click);
            this.lbxServ.DoubleClick += new System.EventHandler(this.lbxServ_DoubleClick);
            // 
            // FrmServidores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 290);
            this.Controls.Add(this.lbxServ);
            this.Controls.Add(this.btnEliminaServ);
            this.Controls.Add(this.btnAddServ);
            this.Controls.Add(this.txtServ);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(422, 329);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(422, 329);
            this.Name = "FrmServidores";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lista Servidores";
            this.Load += new System.EventHandler(this.FrmServidores_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnEliminaServ;
        private System.Windows.Forms.Button btnAddServ;
        private System.Windows.Forms.TextBox txtServ;
        private System.Windows.Forms.ListBox lbxServ;
    }
}