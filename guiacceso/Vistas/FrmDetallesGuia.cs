﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;
using System.Net.Mime;



namespace guiacceso.Vistas
{
    public partial class FrmDetallesGuia : Form
    {
        private string To;
        private string Subject;
        private string Body;
        private MailMessage mail;   
        public FrmDetallesGuia(Image img)
        {
            InitializeComponent();
            pcbImagen.Image = img;
        }

        private void btnImp_Click(object sender, EventArgs e)
        {            
            PrintDialog pd = new PrintDialog();
            if (pd.ShowDialog() == DialogResult.OK)
                printDocument1.Print();

        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(pcbImagen.Image, 10, 40, 805, 1100);
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SaveFileDialog Guardar = new SaveFileDialog();
            Guardar.Filter = "JPEG(*.JPG)|*.JPG";
            Image Imagen = pcbImagen.Image;
            Guardar.ShowDialog();
            Imagen.Save(Guardar.FileName);
        }

        private void FrmDetallesGuia_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar==Convert.ToChar(Keys.Escape))
            {
                this.Close();
            }
        }
    }

}

