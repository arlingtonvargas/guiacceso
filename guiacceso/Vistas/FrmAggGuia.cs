﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using guiacceso.Clases;
using System.Drawing.Printing;
using WIA;
using System.Runtime.InteropServices;


namespace guiacceso.Vistas
{
    public partial class FrmAggGuia : Form
    {
        public FrmAggGuia()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if(ValidaCampos())
            {
                try
                {
                    
                    System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();

                    // Estableciento propiedades
                    cmd.Connection = Funciones.f.Conexion;
                    cmd.CommandText = "set dateformat dmy INSERT INTO EN_Guias VALUES (@codGuia, @scanGuia, @usuRegistra, @fechaReg)";

                    // Creando los parámetros necesarios
                    cmd.Parameters.Add("@codGuia", System.Data.SqlDbType.Text);
                    cmd.Parameters.Add("@scanGuia", System.Data.SqlDbType.Image);
                    cmd.Parameters.Add("@usuRegistra", System.Data.SqlDbType.Text);
                    cmd.Parameters.Add("@fechaReg", System.Data.SqlDbType.DateTime);
                    

                    // Asignando los valores a los atributos
                    cmd.Parameters["@codGuia"].Value = txtGuia.Text;
                    cmd.Parameters["@usuRegistra"].Value = Clases.Funciones.NomUsu;
                    cmd.Parameters["@fechaReg"].Value = DateTime.Now;                
                    // Asignando el valor de la imagen

                    // Stream usado como buffer
                    System.IO.MemoryStream ms = new System.IO.MemoryStream();
                    // Se guarda la imagen en el buffer
                    pcbImagen.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    // Se extraen los bytes del buffer para asignarlos como valor para el 
                    // parámetro.
                    cmd.Parameters["@scanGuia"].Value = ms.GetBuffer();
                    cmd.ExecuteNonQuery();
                    Funciones.msgExitoso("Información guardada exitosamente!");
                    btnLimpiar_Click(sender, e);
                }
                catch (System.Exception ex)
                {
                    Funciones.msgError(ex.Message);
                }
            }
        }
        private bool ValidaCampos()
        {
            try
            {
                if(txtGuia.Text=="")
                {
                    Clases.Funciones.msgError("El numero de la guia no puede estar vacio!");
                    txtGuia.Focus();
                    return false;
                }
                else if(pcbImagen.Image==null)
                {
                    Clases.Funciones.msgError("La imagen de la guia no puede estar vacia!");
                    btnImagen.Focus();
                    return false;
                }
                else { return true; }
            }
            catch (Exception ex)
            {
                Clases.Funciones.msgError(ex.Message,"Mensaje de error :(");
                return false;
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtGuia.Text = "";
            pcbImagen.Image = null;
            txtGuia.Focus();
        }

        private void btnQuitarImg_Click(object sender, EventArgs e)
        {
            pcbImagen.Image = null;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnImagen_Click(object sender, EventArgs e)
        {
            OpenFileDialog BuscarImagen = new OpenFileDialog(); BuscarImagen.Filter = "Archivos de Imagen|*.jpg";
            //Aquí incluiremos los filtros que queramos.
            BuscarImagen.FileName = "";
            BuscarImagen.Title = "Cargar imagen de guia";
            if (BuscarImagen.ShowDialog() == DialogResult.OK)
            {
                /// Si esto se cumple, capturamos la propiedad File Name y la guardamos en el control

                String Direccion = BuscarImagen.FileName;
                this.pcbImagen.ImageLocation = Direccion;
                //Pueden usar tambien esta forma para cargar la Imagen solo activenla y comenten la linea donde se cargaba anteriormente 
                //this.pictureBox1.ImageLocation = textBox1.Text;
                pcbImagen.SizeMode = PictureBoxSizeMode.CenterImage;
            }
        }

        private void FrmAggGuia_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                Funciones.ValidaMdiParent();
            }
            catch (Exception ex)
            {
                Funciones.msgError(ex.Message);
            }
        }
    }
}
