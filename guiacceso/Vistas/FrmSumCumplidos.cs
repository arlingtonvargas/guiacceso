﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace guiacceso.Vistas
{
    
    public partial class FrmSumCumplidos : Form
    {
        string titulo;
        int val;
        public FrmSumCumplidos(Int64 total,string rango)
        {
            InitializeComponent();
            lblTitle.Text = rango;
            lblTotal.Text = string.Format("Total Guias {0}",total);
        
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
