﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using guiacceso.Clases;

namespace guiacceso.Vistas
{
    public partial class FrmLogin : Form
    {
        bool ingresaTxtUser = false;
        bool ingresaTxtClave = false;
        bool ingresaServer = false;
        //Clases.Funciones f = new Clases.Funciones();
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            try
            {
                if (Funciones.listaServidores.Count > 0) { txtServer.Text = Funciones.listaServidores[Funciones.listaServidores.Count - 1]; }
            }
            catch (Exception ex)
            {
                Funciones.msgError(ex.Message);
            }
        }

        private void txtUsuario_Enter(object sender, EventArgs e)
        {
            if (!ingresaTxtUser)
            {
                ingresaTxtUser = true;
                LimpiaTxtMarcaAgua(ref txtUsuario);
            }

        }

        private void LimpiaTxtMarcaAgua(ref TextBox txt)
        {
            txt.Font = new Font("Segoe UI", txt.Font.Size, FontStyle.Regular);
            txt.ForeColor = Color.Black;
            txt.BackColor = Color.LightCyan;
            if(txt.Name == "txtClave")
            {
                txt.PasswordChar = '*';
            }
            if (txt.Name != "txtServer" && txt.Text!="Servidor")
            {                
                txt.Text = "";
            }
            if (txt.Name == "txtServer" && txt.Text == "Servidor")
            {
                txt.Text = "";
            }
        }
        private void FormatTxtMarcaAgua(ref TextBox txt)
        {
            txt.Font = new Font("Segoe UI", txt.Font.Size, FontStyle.Italic);
            txt.ForeColor = SystemColors.ScrollBar;
            txt.BackColor = Color.White;
            if (txt.Name == "txtUsuario")
            {
                txt.Text = "Usuario";
            }
            else if (txt.Name=="txtClave")
            {
                txt.Text = "Contraseña";
                txt.PasswordChar = '\0';
            }else if(txt.Name=="txtServer")
            {
                txt.Text = "Servidor";
            }

        }
        private void txtUsuario_Leave(object sender, EventArgs e)
        {
            if (txtUsuario.Text == "")
            {
                FormatTxtMarcaAgua(ref txtUsuario);
                ingresaTxtUser = false;
            }else { txtUsuario.BackColor = Color.White; }
        }

        private void txtClave_Enter(object sender, EventArgs e)
        {
            if (!ingresaTxtClave)
            {
                ingresaTxtClave = true;
                LimpiaTxtMarcaAgua(ref txtClave);
            }
        }

        private void txtClave_Leave(object sender, EventArgs e)
        {
            if (txtClave.Text == "")
            {
                FormatTxtMarcaAgua(ref txtClave);
                ingresaTxtClave = false;
            }else { txtClave.BackColor = Color.White; }
        }

        private void btnIngreso_Click(object sender, EventArgs e)
        {
            if (ValidaCampos())
            {

                if (Clases.Funciones.f.Ingreso(txtServer.Text))
                {
                    string sql = string.Format("SELECT * FROM EN_Ususario WHERE NomUsu='{0}' AND Clave='{1}'", txtUsuario.Text, txtClave.Text);
                    DataTable dt = Clases.Funciones.f.AbrirTabla(Clases.Funciones.f.Conexion, sql);
                    if (dt.Rows.Count > 0)
                    {
                        Funciones.msgExitoso("Bienvenido al sistema...");
                        Clases.Funciones.f.EstaConectado = true;
                        Clases.Funciones.NomUsu = txtUsuario.Text;
                        this.Close();
                    }
                    else
                    {
                        Funciones.msgError("Usuario o Contraseña Incorrectos!!");
                        Clases.Funciones.f.EstaConectado = false;
                        txtClave.Text = "";
                        txtClave.Focus();
                        Clases.Funciones.f.DesconectaBD();
                    }
                }
            }
        }
        private bool ValidaCampos()
        {
            if (txtServer.Text == "" || txtServer.Text == "Servidor")
            {
                Funciones.msgError("Lo sentimos, al parecer el campo servidor se encuentra vacio! :/");
                txtServer.Focus();
                return false;
            }
            else if (txtUsuario.Text == "" || txtUsuario.Text == "Usuario")
            {
                Funciones.msgError("Lo sentimos, al parecer el nombre de usuario se encuentra vacio! :/");
                txtUsuario.Focus();
                return false;
            }
            else if (txtClave.Text == "" || txtClave.Text == "Contraseña")
            {
                Funciones.msgError("Lo sentimos, al parecer la contraseña se encuentra vacia! :/");
                txtClave.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }

        private void txtUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            Clases.Funciones.f.AvanzaConEnter(e);
        }

        private void txtClave_KeyPress(object sender, KeyPressEventArgs e)
        {
            Clases.Funciones.f.AvanzaConEnter(e);
        }

        private void txtServer_Enter(object sender, EventArgs e)
        {
            if (!ingresaServer)
            {
                ingresaServer = true;
                LimpiaTxtMarcaAgua(ref txtServer);
            }
        }

        private void txtServer_Leave(object sender, EventArgs e)
        {
            if (txtServer.Text == "")
            {
                FormatTxtMarcaAgua(ref txtServer);
                ingresaServer = false;
            }else { txtServer.BackColor = Color.White; }
        }

        private void txtServer_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funciones.f.AvanzaConEnter(e);
        }

        private void btnVerServ_Click(object sender, EventArgs e)
        {
            try
            {
                Form f = new FrmServidores(ref this.txtServer);
                f.ShowDialog();
                txtServer.Focus();
            }
            catch (Exception ex)
            {
                Funciones.msgError(ex.Message);
            }
        }
    }
}
