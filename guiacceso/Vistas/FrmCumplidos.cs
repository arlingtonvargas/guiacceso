﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using guiacceso.Clases;

namespace guiacceso.Vistas
{
    public partial class FrmCumplidos : Form
    {
        public FrmCumplidos()
        {
            InitializeComponent();
        }

   
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if(ValidaCampos())
                {

                    System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();

                    // Estableciento propiedades
                    cmd.Connection = Funciones.f.Conexion;
                    cmd.CommandText = "set dateformat dmy INSERT INTO EN_Cumplidos VALUES (@FechaReg,@CantGuias,@UsuReg)";

                    // Creando los parámetros necesarios             
                    cmd.Parameters.Add("@FechaReg", System.Data.SqlDbType.DateTime);
                    cmd.Parameters.Add("@CantGuias", System.Data.SqlDbType.Int);
                    cmd.Parameters.Add("@UsuReg", System.Data.SqlDbType.Text);


                    // Asignando los valores a los atributos                   
                    cmd.Parameters["@FechaReg"].Value = dtpFecha.Value;
                    cmd.Parameters["@CantGuias"].Value = txtCant.Text;
                    cmd.Parameters["@UsuReg"].Value = Clases.Funciones.NomUsu;
                    cmd.ExecuteNonQuery();
                    Funciones.msgExitoso("Información guardada exitosamente!");
                    LimpiarCampos();
                    gvCumplidos.Rows.Clear();
                    LlenaGrilla();
                }
            }
            catch (Exception ex)
            {
                Funciones.msgError(ex.Message);
            }
        }
        private bool ValidaCampos()
        {
            try
            {
                if(dtpFecha.Text=="")
                {
                    Funciones.msgError("El campo fecha no puede estar vacio!");
                    dtpFecha.Focus();
                    return false;
                }else if(txtCant.Text=="")
                {
                    Funciones.msgError("El campo cantidad no puede estar vacio!");
                    txtCant.Focus();
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Funciones.msgError(ex.Message);
                return false;
            }
        }
        private void LimpiarCampos()
        {
            dtpFecha.Value = DateTime.Now;
            txtCant.Text = "";
            dtpFecha.Focus();
        }        

        private void LlenaGrilla()
        {
            try
            {
                string sql = "SELECT * FROM EN_Cumplidos";

               DataTable  dtConsulta  = Funciones.f.AbrirTabla(Funciones.f.Conexion, sql);
                //gvCumplidos.DataSource = dtConsulta;         
                for (int i = 0; i < dtConsulta.Rows.Count; i++)
                {
                    gvCumplidos.Rows.Add(1);
                    gvCumplidos.Rows[i].Cells[0].Value = dtConsulta.Rows[i]["Sec"];
                    gvCumplidos.Rows[i].Cells[1].Value = dtConsulta.Rows[i]["FechaReg"];
                    gvCumplidos.Rows[i].Cells[2].Value = dtConsulta.Rows[i]["CantGuias"];
                    gvCumplidos.Rows[i].Cells[3].Value = dtConsulta.Rows[i]["UsuReg"];
                }
            }
            catch (Exception ex)
            {
                Funciones.msgError(ex.Message);
            }
        }

        private void FrmCumplidos_Load(object sender, EventArgs e)
        {
            LlenaGrilla();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmCumplidos_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                Funciones.ValidaMdiParent();
            }
            catch (Exception ex)
            {
                Funciones.msgError(ex.Message);
            }
        }
    }
}
