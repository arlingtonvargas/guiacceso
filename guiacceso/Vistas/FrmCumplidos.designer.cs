﻿namespace guiacceso.Vistas
{
    partial class FrmCumplidos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCumplidos));
            this.gvCumplidos = new System.Windows.Forms.DataGridView();
            this.Sec = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaReg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CantGuias = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UsuReg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblFecha = new System.Windows.Forms.Label();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.lblCant = new System.Windows.Forms.Label();
            this.txtCant = new System.Windows.Forms.TextBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.txtBusqueda = new System.Windows.Forms.TextBox();
            this.gbxInf = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.gvCumplidos)).BeginInit();
            this.gbxInf.SuspendLayout();
            this.SuspendLayout();
            // 
            // gvCumplidos
            // 
            this.gvCumplidos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvCumplidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvCumplidos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Sec,
            this.FechaReg,
            this.CantGuias,
            this.UsuReg});
            this.gvCumplidos.Location = new System.Drawing.Point(12, 96);
            this.gvCumplidos.Name = "gvCumplidos";
            this.gvCumplidos.Size = new System.Drawing.Size(834, 343);
            this.gvCumplidos.TabIndex = 0;
            // 
            // Sec
            // 
            this.Sec.HeaderText = "Sec";
            this.Sec.Name = "Sec";
            this.Sec.ReadOnly = true;
            this.Sec.Visible = false;
            // 
            // FechaReg
            // 
            this.FechaReg.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FechaReg.HeaderText = "Fecha";
            this.FechaReg.Name = "FechaReg";
            this.FechaReg.ReadOnly = true;
            // 
            // CantGuias
            // 
            this.CantGuias.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CantGuias.HeaderText = "Cantidad Guias";
            this.CantGuias.Name = "CantGuias";
            this.CantGuias.ReadOnly = true;
            // 
            // UsuReg
            // 
            this.UsuReg.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.UsuReg.HeaderText = "Usuario Reg";
            this.UsuReg.Name = "UsuReg";
            this.UsuReg.ReadOnly = true;
            // 
            // lblFecha
            // 
            this.lblFecha.Location = new System.Drawing.Point(8, 23);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(69, 17);
            this.lblFecha.TabIndex = 14;
            this.lblFecha.Text = "Fecha : ";
            this.lblFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpFecha
            // 
            this.dtpFecha.CustomFormat = "dd/MM/yyyy hh:mm tt";
            this.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecha.Location = new System.Drawing.Point(79, 20);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(90, 23);
            this.dtpFecha.TabIndex = 13;
            // 
            // lblCant
            // 
            this.lblCant.Location = new System.Drawing.Point(173, 22);
            this.lblCant.Name = "lblCant";
            this.lblCant.Size = new System.Drawing.Size(118, 17);
            this.lblCant.TabIndex = 16;
            this.lblCant.Text = "Cantidad de Guias : ";
            this.lblCant.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCant
            // 
            this.txtCant.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCant.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCant.Location = new System.Drawing.Point(297, 18);
            this.txtCant.Name = "txtCant";
            this.txtCant.Size = new System.Drawing.Size(67, 25);
            this.txtCant.TabIndex = 17;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.Image")));
            this.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardar.Location = new System.Drawing.Point(371, 18);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(83, 25);
            this.btnGuardar.TabIndex = 18;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalir.Location = new System.Drawing.Point(460, 18);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(77, 25);
            this.btnSalir.TabIndex = 19;
            this.btnSalir.Text = "Salir";
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // txtBusqueda
            // 
            this.txtBusqueda.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBusqueda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBusqueda.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBusqueda.Location = new System.Drawing.Point(12, 63);
            this.txtBusqueda.Name = "txtBusqueda";
            this.txtBusqueda.Size = new System.Drawing.Size(834, 33);
            this.txtBusqueda.TabIndex = 20;
            this.txtBusqueda.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // gbxInf
            // 
            this.gbxInf.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxInf.Controls.Add(this.dtpFecha);
            this.gbxInf.Controls.Add(this.lblFecha);
            this.gbxInf.Controls.Add(this.btnSalir);
            this.gbxInf.Controls.Add(this.lblCant);
            this.gbxInf.Controls.Add(this.btnGuardar);
            this.gbxInf.Controls.Add(this.txtCant);
            this.gbxInf.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxInf.Location = new System.Drawing.Point(12, 12);
            this.gbxInf.Name = "gbxInf";
            this.gbxInf.Size = new System.Drawing.Size(834, 50);
            this.gbxInf.TabIndex = 21;
            this.gbxInf.TabStop = false;
            this.gbxInf.Text = "Información";
            // 
            // FrmCumplidos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(858, 451);
            this.Controls.Add(this.gbxInf);
            this.Controls.Add(this.txtBusqueda);
            this.Controls.Add(this.gvCumplidos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmCumplidos";
            this.Text = "Cumplidos diarios";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmCumplidos_FormClosed);
            this.Load += new System.EventHandler(this.FrmCumplidos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gvCumplidos)).EndInit();
            this.gbxInf.ResumeLayout(false);
            this.gbxInf.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView gvCumplidos;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.Label lblCant;
        private System.Windows.Forms.TextBox txtCant;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnSalir;
        internal System.Windows.Forms.TextBox txtBusqueda;
        private System.Windows.Forms.GroupBox gbxInf;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sec;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaReg;
        private System.Windows.Forms.DataGridViewTextBoxColumn CantGuias;
        private System.Windows.Forms.DataGridViewTextBoxColumn UsuReg;
    }
}