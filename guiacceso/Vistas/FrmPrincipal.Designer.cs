﻿namespace guiacceso
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrincipal));
            this.menuPrincipal = new System.Windows.Forms.MenuStrip();
            this.menuArchivo = new System.Windows.Forms.ToolStripMenuItem();
            this.sbtnIniciar = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAcercaDe = new System.Windows.Forms.ToolStripMenuItem();
            this.menuGuias = new System.Windows.Forms.ToolStripMenuItem();
            this.sbtnRegGuia = new System.Windows.Forms.ToolStripMenuItem();
            this.sbtnConGuia = new System.Windows.Forms.ToolStripMenuItem();
            this.BarraBotones = new System.Windows.Forms.ToolStrip();
            this.btnIniciar = new System.Windows.Forms.ToolStripButton();
            this.btnRegGuia = new System.Windows.Forms.ToolStripButton();
            this.btnConsultaGuia = new System.Windows.Forms.ToolStripButton();
            this.btnCumplidos = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnRegCumplido = new System.Windows.Forms.ToolStripMenuItem();
            this.btnConsultaCump = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSalir = new System.Windows.Forms.ToolStripButton();
            this.Imagenes16X16 = new System.Windows.Forms.ImageList(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.menuCumplidos = new System.Windows.Forms.ToolStripMenuItem();
            this.sbtnRegCumplido = new System.Windows.Forms.ToolStripMenuItem();
            this.sbtnConsultaCump = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPrincipal.SuspendLayout();
            this.BarraBotones.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuPrincipal
            // 
            this.menuPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuArchivo,
            this.menuGuias,
            this.menuCumplidos});
            this.menuPrincipal.Location = new System.Drawing.Point(0, 0);
            this.menuPrincipal.Name = "menuPrincipal";
            this.menuPrincipal.Size = new System.Drawing.Size(946, 24);
            this.menuPrincipal.TabIndex = 3;
            this.menuPrincipal.Text = "menuStrip1";
            // 
            // menuArchivo
            // 
            this.menuArchivo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sbtnIniciar,
            this.btnAcercaDe});
            this.menuArchivo.Name = "menuArchivo";
            this.menuArchivo.Size = new System.Drawing.Size(60, 20);
            this.menuArchivo.Text = "Archivo";
            // 
            // sbtnIniciar
            // 
            this.sbtnIniciar.Name = "sbtnIniciar";
            this.sbtnIniciar.Size = new System.Drawing.Size(126, 22);
            this.sbtnIniciar.Text = "Iniciar";
            this.sbtnIniciar.Click += new System.EventHandler(this.btnIniciar_Click);
            // 
            // btnAcercaDe
            // 
            this.btnAcercaDe.Name = "btnAcercaDe";
            this.btnAcercaDe.Size = new System.Drawing.Size(126, 22);
            this.btnAcercaDe.Text = "Acerca de";
            this.btnAcercaDe.Click += new System.EventHandler(this.btnAcercaDe_Click);
            // 
            // menuGuias
            // 
            this.menuGuias.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sbtnRegGuia,
            this.sbtnConGuia});
            this.menuGuias.Name = "menuGuias";
            this.menuGuias.Size = new System.Drawing.Size(48, 20);
            this.menuGuias.Text = "Guias";
            // 
            // sbtnRegGuia
            // 
            this.sbtnRegGuia.Name = "sbtnRegGuia";
            this.sbtnRegGuia.Size = new System.Drawing.Size(152, 22);
            this.sbtnRegGuia.Text = "Registrar";
            this.sbtnRegGuia.Click += new System.EventHandler(this.btnRegGuia_Click);
            // 
            // sbtnConGuia
            // 
            this.sbtnConGuia.Name = "sbtnConGuia";
            this.sbtnConGuia.Size = new System.Drawing.Size(152, 22);
            this.sbtnConGuia.Text = "Consultar";
            this.sbtnConGuia.Click += new System.EventHandler(this.btnConsultaGuia_Click);
            // 
            // BarraBotones
            // 
            this.BarraBotones.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnIniciar,
            this.btnRegGuia,
            this.btnConsultaGuia,
            this.btnCumplidos,
            this.btnSalir});
            this.BarraBotones.Location = new System.Drawing.Point(0, 24);
            this.BarraBotones.Name = "BarraBotones";
            this.BarraBotones.Size = new System.Drawing.Size(946, 38);
            this.BarraBotones.TabIndex = 4;
            this.BarraBotones.Text = "toolStrip1";
            // 
            // btnIniciar
            // 
            this.btnIniciar.Image = ((System.Drawing.Image)(resources.GetObject("btnIniciar.Image")));
            this.btnIniciar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnIniciar.Name = "btnIniciar";
            this.btnIniciar.Size = new System.Drawing.Size(43, 35);
            this.btnIniciar.Text = "Iniciar";
            this.btnIniciar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnIniciar.Click += new System.EventHandler(this.btnIniciar_Click);
            // 
            // btnRegGuia
            // 
            this.btnRegGuia.Image = ((System.Drawing.Image)(resources.GetObject("btnRegGuia.Image")));
            this.btnRegGuia.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRegGuia.Name = "btnRegGuia";
            this.btnRegGuia.Size = new System.Drawing.Size(84, 35);
            this.btnRegGuia.Text = "Registrar Guia";
            this.btnRegGuia.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnRegGuia.Click += new System.EventHandler(this.btnRegGuia_Click);
            // 
            // btnConsultaGuia
            // 
            this.btnConsultaGuia.Image = ((System.Drawing.Image)(resources.GetObject("btnConsultaGuia.Image")));
            this.btnConsultaGuia.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnConsultaGuia.Name = "btnConsultaGuia";
            this.btnConsultaGuia.Size = new System.Drawing.Size(89, 35);
            this.btnConsultaGuia.Text = "Consultar Guia";
            this.btnConsultaGuia.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnConsultaGuia.Click += new System.EventHandler(this.btnConsultaGuia_Click);
            // 
            // btnCumplidos
            // 
            this.btnCumplidos.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnRegCumplido,
            this.btnConsultaCump});
            this.btnCumplidos.Image = ((System.Drawing.Image)(resources.GetObject("btnCumplidos.Image")));
            this.btnCumplidos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCumplidos.Name = "btnCumplidos";
            this.btnCumplidos.Size = new System.Drawing.Size(78, 35);
            this.btnCumplidos.Text = "Cumplidos";
            this.btnCumplidos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // btnRegCumplido
            // 
            this.btnRegCumplido.Name = "btnRegCumplido";
            this.btnRegCumplido.Size = new System.Drawing.Size(152, 22);
            this.btnRegCumplido.Text = "Registrar";
            this.btnRegCumplido.Click += new System.EventHandler(this.btnRegCumplido_Click);
            // 
            // btnConsultaCump
            // 
            this.btnConsultaCump.Name = "btnConsultaCump";
            this.btnConsultaCump.Size = new System.Drawing.Size(152, 22);
            this.btnConsultaCump.Text = "Consultar";
            this.btnConsultaCump.Click += new System.EventHandler(this.btnConsultaCump_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(33, 35);
            this.btnSalir.Text = "Salir";
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // Imagenes16X16
            // 
            this.Imagenes16X16.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Imagenes16X16.ImageStream")));
            this.Imagenes16X16.TransparentColor = System.Drawing.Color.Transparent;
            this.Imagenes16X16.Images.SetKeyName(0, "PrintSortAsc_16x16.png");
            this.Imagenes16X16.Images.SetKeyName(1, "PrintSortDesc_16x16.png");
            this.Imagenes16X16.Images.SetKeyName(2, "AddFooter_16x16.png");
            this.Imagenes16X16.Images.SetKeyName(3, "FindByID_16x16.png");
            this.Imagenes16X16.Images.SetKeyName(4, "Close_16x16.png");
            this.Imagenes16X16.Images.SetKeyName(5, "Task_16x16.png");
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 441);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(946, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // menuCumplidos
            // 
            this.menuCumplidos.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sbtnRegCumplido,
            this.sbtnConsultaCump});
            this.menuCumplidos.Name = "menuCumplidos";
            this.menuCumplidos.Size = new System.Drawing.Size(77, 20);
            this.menuCumplidos.Text = "Cumplidos";
            // 
            // sbtnRegCumplido
            // 
            this.sbtnRegCumplido.Name = "sbtnRegCumplido";
            this.sbtnRegCumplido.Size = new System.Drawing.Size(152, 22);
            this.sbtnRegCumplido.Text = "Registrar";
            this.sbtnRegCumplido.Click += new System.EventHandler(this.btnRegCumplido_Click);
            // 
            // sbtnConsultaCump
            // 
            this.sbtnConsultaCump.Name = "sbtnConsultaCump";
            this.sbtnConsultaCump.Size = new System.Drawing.Size(152, 22);
            this.sbtnConsultaCump.Text = "Consultar";
            this.sbtnConsultaCump.Click += new System.EventHandler(this.btnConsultaCump_Click);
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(946, 463);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.BarraBotones);
            this.Controls.Add(this.menuPrincipal);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmPrincipal";
            this.Text = "Cumplidos Guias RapiEntrega";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmPrincipal_FormClosing);
            this.Load += new System.EventHandler(this.FrmPrincipal_Load);
            this.menuPrincipal.ResumeLayout(false);
            this.menuPrincipal.PerformLayout();
            this.BarraBotones.ResumeLayout(false);
            this.BarraBotones.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuPrincipal;
        private System.Windows.Forms.ToolStripMenuItem menuArchivo;
        private System.Windows.Forms.ToolStripMenuItem sbtnIniciar;
        private System.Windows.Forms.ToolStripMenuItem btnAcercaDe;
        private System.Windows.Forms.ToolStripMenuItem menuGuias;
        private System.Windows.Forms.ToolStripMenuItem sbtnRegGuia;
        private System.Windows.Forms.ToolStripMenuItem sbtnConGuia;
        private System.Windows.Forms.ToolStrip BarraBotones;
        private System.Windows.Forms.ToolStripButton btnSalir;
        private System.Windows.Forms.ToolStripButton btnIniciar;
        private System.Windows.Forms.ToolStripButton btnRegGuia;
        public System.Windows.Forms.ImageList Imagenes16X16;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripButton btnConsultaGuia;
        private System.Windows.Forms.ToolStripDropDownButton btnCumplidos;
        private System.Windows.Forms.ToolStripMenuItem btnRegCumplido;
        private System.Windows.Forms.ToolStripMenuItem btnConsultaCump;
        private System.Windows.Forms.ToolStripMenuItem menuCumplidos;
        private System.Windows.Forms.ToolStripMenuItem sbtnRegCumplido;
        private System.Windows.Forms.ToolStripMenuItem sbtnConsultaCump;
    }
}

