﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using guiacceso.Clases;

namespace guiacceso.Vistas
{
    public partial class FrmTotalizaCumplidos : Form
    {
        DataTable dtConsulta;
        public FrmTotalizaCumplidos()
        {
            InitializeComponent();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            gvCumplidos.Rows.Clear();
            CargarDatos();
        }
        private void CargarDatos()
        {
            try
            {
                string sql =  string.Format("set dateformat dmy " +
                " SELECT* FROM  EN_Cumplidos WHERE FechaReg BETWEEN '{0:dd/MM/yyyy HH:mm:ss}' AND '{1:dd/MM/yyyy  HH:mm:ss}'", dtpFecDesde.Value, dtpFecHasta.Value);
                dtConsulta = Funciones.f.AbrirTabla(Funciones.f.Conexion, sql);
                //gvCumplidos.DataSource = dtConsulta;
                if (dtConsulta.Rows.Count == 0)
                {
                    Funciones.msgExitoso("No se encontraron Registros :(");
                    return;
                }
                for (int i = 0; i < dtConsulta.Rows.Count; i++)
                {
                    gvCumplidos.Rows.Add(1);
                    gvCumplidos.Rows[i].Cells[0].Value = dtConsulta.Rows[i]["Sec"];
                    gvCumplidos.Rows[i].Cells[1].Value = dtConsulta.Rows[i]["FechaReg"];
                    gvCumplidos.Rows[i].Cells[2].Value = dtConsulta.Rows[i]["CantGuias"];
                    gvCumplidos.Rows[i].Cells[3].Value = dtConsulta.Rows[i]["UsuReg"];
                }

            }
            catch (Exception ex)
            {
                Funciones.msgError(ex.Message);
            }
        }

        private void gvGuias_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnTotaliza_Click(object sender, EventArgs e)
        {
            if (gvCumplidos.Rows.Count <= 1) { return; }
            object sumObject;
            sumObject = dtConsulta.Compute("Sum(CantGuias)", "");
            string rango = string.Format("Guias cumplidas entre {0:dd/MM/yyyy} y el {1:dd/MM/yyyy}", dtpFecDesde.Value,dtpFecHasta.Value);
            Form f = new FrmSumCumplidos(Convert.ToInt64(sumObject), rango);
            f.ShowDialog();
        }

        private void FrmTotalizaCumplidos_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                Funciones.ValidaMdiParent();
            }
            catch (Exception ex)
            {
                Funciones.msgError(ex.Message);
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
