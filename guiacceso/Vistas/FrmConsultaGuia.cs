﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using guiacceso.Clases;

namespace guiacceso.Vistas
{
    public partial class FrmConsultaGuia : Form
    {
        DataTable dtConsulta;
        public FrmConsultaGuia()
        {
            InitializeComponent();
        }

        private void FrmConsultaGuia_Load(object sender, EventArgs e)
        {
            //CargarDatos();
            gvGuias.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }
        private void CargarDatos(bool conFecha)
        {
            try
            {
                string sql = "";
                if (conFecha)
                {
                    sql = string.Format("set dateformat dmy " +
                " SELECT* FROM  EN_Guias WHERE fechaReg BETWEEN '{0:dd/MM/yyyy HH:mm:ss}' AND '{1:dd/MM/yyyy  HH:mm:ss}'", dtpFechaDesde.Value, dtpFechaHasta.Value);
                }
                else
                {
                    sql = string.Format("set dateformat dmy SELECT* FROM  EN_Guias");
                }
                
                dtConsulta = Funciones.f.AbrirTabla(Funciones.f.Conexion,sql);
                //gvGuias.DataSource = dtConsulta;
                if (dtConsulta.Rows.Count == 0)
                {
                    Funciones.msgExitoso("No se encontraron Registros :(");
                    return;
                }
                for (int i = 0; i < dtConsulta.Rows.Count; i++)
                {
                    gvGuias.Rows.Add(1);
                    gvGuias.Rows[i].Cells[0].Value = dtConsulta.Rows[i]["Sec"];
                    gvGuias.Rows[i].Cells[1].Value = dtConsulta.Rows[i]["codGuia"];
                    gvGuias.Rows[i].Cells[4].Value = dtConsulta.Rows[i]["scanGuia"];
                    gvGuias.Rows[i].Cells[3].Value = dtConsulta.Rows[i]["usuRegistra"];
                    gvGuias.Rows[i].Cells[2].Value = dtConsulta.Rows[i]["fechaReg"];
                }
               
            }
            catch (Exception ex)
            {
                Funciones.msgError(ex.Message);
            }
        }

        private void gvGuias_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void gvGuias_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
           

            try
            {
                if (e.RowIndex>=gvGuias.RowCount-1 || e.RowIndex==-1) { return; }
                // El campo productImage primero se almacena en un buffer
                byte[] imageBuffer = (byte[])gvGuias.Rows[e.RowIndex].Cells["scanGuia"].Value;
                // Se crea un MemoryStream a partir de ese buffer
                System.IO.MemoryStream ms = new System.IO.MemoryStream(imageBuffer);
                // Se utiliza el MemoryStream para extraer la imagen
                Image img = Image.FromStream(ms);
                FrmDetallesGuia frm = new FrmDetallesGuia(img);
                frm.ShowDialog();

            }
            catch (System.Exception ex)
            {
                Funciones.msgError(ex.Message);
            }
        }

        private void txtBusqueda_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //FiltrarDatos(txtBusqueda.Text);
                string Filtro = "";
                DataRow[] Filas = null;
                if (txtBusqueda.Text.Length > 0)
                {
                    Filtro = CrearFiltro(dtConsulta, txtBusqueda.Text);
                }

                Filas = dtConsulta.Select(Filtro);
                dynamic Tb1 = dtConsulta.Clone();
                int i = 0;
                for (i = 0; i <= Filas.GetUpperBound(0); i++)
                {
                    Tb1.ImportRow(Filas[i]);
                }
                gvGuias.Rows.Clear();
                for (i = 0; i <= Tb1.Rows.Count - 1; i++)
                {
                    gvGuias.Rows.Add(1);
                    gvGuias.Rows[i].Cells[0].Value = Tb1.Rows[i]["Sec"];
                    gvGuias.Rows[i].Cells[1].Value = Tb1.Rows[i]["codGuia"];
                    gvGuias.Rows[i].Cells[4].Value = Tb1.Rows[i]["scanGuia"];
                    gvGuias.Rows[i].Cells[3].Value = Tb1.Rows[i]["usuRegistra"];
                    gvGuias.Rows[i].Cells[2].Value = Tb1.Rows[i]["fechaReg"];
                }
            }
            catch (Exception ex)
            {

                Funciones.msgError(ex.Message);
            }
        }

        private void FiltrarDatos(string filtro)
        {
            //gvGuias.DataSource = new DataTable();
            DataView dv = new DataView(dtConsulta);
            dv.RowFilter = CrearFiltro1(dtConsulta, filtro);
            //gvGuias.DataSource = dv;
            DataTable dt = dv.ToTable();
            gvGuias.Rows.Clear();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                gvGuias.Rows.Add(1);
                gvGuias.Rows[i].Cells[0].Value = dtConsulta.Rows[i]["Sec"];
                gvGuias.Rows[i].Cells[1].Value = dtConsulta.Rows[i]["codGuia"];
                gvGuias.Rows[i].Cells[4].Value = dtConsulta.Rows[i]["scanGuia"];
                gvGuias.Rows[i].Cells[3].Value = dtConsulta.Rows[i]["usuRegistra"];
                gvGuias.Rows[i].Cells[2].Value = dtConsulta.Rows[i]["fechaReg"];
            }
        }

        public static string CrearFiltro1(DataTable tabla, string texto)
        {
            texto = texto.Replace(' ', ';');
            string[] cadenas = texto.Split(';');
            string filtro = "";
            for (int i = 0; i < cadenas.Length; i++)
            {
                if (cadenas[i] != "" && cadenas[i] != " ")
                {
                    if (i > 0)
                        filtro += " and ";
                    filtro += " ( ";
                    for (int o = 0; o < tabla.Columns.Count; o++)
                    {
                        filtro += " CONVERT(" + tabla.Columns[o].ColumnName + ", System.String) like '%" + cadenas[i] + "%' ";
                        if (o < tabla.Columns.Count - 1)
                            filtro += " or ";
                    }
                    filtro += " ) ";
                }
            }
            return filtro;
        }

        public static string CrearFiltro(DataTable tabla, string texto)
        {
            if ((tabla == null))
            {
                return "";
            }
            else
            {
                if (tabla.Rows.Count > 0)
                {
                    texto = texto.Replace(' ', ';');
                    string[] cadenas = texto.Split(';');
                    string filtro = "";
                    for (int i = 0; i <= cadenas.Length - 1; i++)
                    {
                        if (!string.IsNullOrEmpty(cadenas[i].Trim()))
                        {
                            if (i > 0)
                            {
                                filtro += " and ";
                            }
                            filtro += "(";
                            for (int o = 0; o <= tabla.Columns.Count - 1; o++)
                            {
                                if (tabla.Columns[o].DataType == Type.GetType("System.String"))
                                {
                                    filtro += " [" + tabla.Columns[o].ColumnName + "] like '%" + cadenas[i] + "%' ";
                                }
                                else if (tabla.Columns[o].DataType == Type.GetType("System.image"))
                                {

                                }
                                else
                                {
                                    filtro += " CONVERT([" + tabla.Columns[o].ColumnName + "], System.String) like '%" + cadenas[i] + "%' ";
                                }
                                if (o < tabla.Columns.Count - 1)
                                {
                                    filtro += " or ";
                                }
                            }
                            filtro += " ) ";
                        }
                    }
                    return filtro;
                }
                else
                {
                    return "";
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            gvGuias.Rows.Clear();
            CargarDatos(true);
        }

        private void FrmConsultaGuia_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                Funciones.ValidaMdiParent();
            }
            catch (Exception ex)
            {
                Funciones.msgError(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            gvGuias.Rows.Clear();
            CargarDatos(false);
        }
    }
}
