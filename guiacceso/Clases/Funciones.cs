﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;

namespace guiacceso.Clases
{
    class Funciones
    {
        public static Funciones f = new Funciones();
        public static FrmPrincipal formPrinci = new FrmPrincipal();
        public static string NomUsu;
        public static List<string> listaServidores = new List<string>();
        bool vEstacConectado = false;


        public enum EnumImagenes16X16
        {
            Conectar,
            Desconectar,
            AgregarGuia,
            BuscarGuia,
            Salir,
            Cumplidos,

        };
        public static Image Imagen_boton16X16(EnumImagenes16X16 NumImg)
        {
            FrmPrincipal frm = new FrmPrincipal();
            Image img = frm.Imagenes16X16.Images[Convert.ToInt16(NumImg)];
            return img;
        }



        public bool EstaConectado
        {
            get
            {
                return vEstacConectado;
            }
            set
            {
                vEstacConectado = value;
            }
        }


        public static string Servidor = "";

        public SqlConnection Conexion;
        //public bool Ingreso(string server)
        //{
        //    try
        //    {

        //        Conexion = new SqlConnection("Data Source=DESKTOP-UTJCUNR\\PONAO;Initial Catalog=RAPIENTREGA;user id = sa; password = 2121121512");
        //        Conexion.Open();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Funciones.msgError(ex.Message);
        //        return false;
        //    }

        //}
        public bool Ingreso(string server)
        {
            try
            {
                Servidor = server;
                Conexion = new SqlConnection(string.Format("Data Source={0};Initial Catalog=RAPIENTREGA;user id = sa; password = 2121121512", Servidor));
                Conexion.Open();
                return true;
            }
            catch (Exception ex)
            {
                Funciones.msgError(ex.Message);
                return false;
            }

        }
        public void DesconectaBD()
        {
            Conexion.Close();
            EstaConectado = false;
        }





        public DataTable AbrirTabla(SqlConnection Con, string SQL)
        {
            try
            {
                SqlCommand Cmd = new SqlCommand();
                DataTable TB = new DataTable();
                SqlDataAdapter Adaptador = default(SqlDataAdapter);
                Cmd.Connection = Con;
                Cmd.CommandText = SQL;
                Cmd.CommandType = CommandType.Text;
                Adaptador = new SqlDataAdapter(Cmd);
                Adaptador.Fill(TB);
                Adaptador.Dispose();
                Cmd.Dispose();
                return TB;
            }
            catch (Exception ex)
            {
                Funciones.msgError(ex.Message);
                return null;
            }

        }
        public void AvanzaConEnter(KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }
        public static void msgError(string Mensaje, string Titulo = "")
        {
            MessageBox.Show(Mensaje, Convert.ToString((!string.IsNullOrEmpty(Titulo) ? Titulo : "Mensaje del Sistema")), MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        public static void msgExitoso(string Mensaje, string Titulo = "")
        {
            MessageBox.Show(Mensaje, Convert.ToString((!string.IsNullOrEmpty(Titulo) ? Titulo : "Mensaje del Sistema")), MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static DialogResult msgResult(string Mensaje, MessageBoxButtons Botones, MessageBoxIcon Icono, string Titulo = "")
        {
            return MessageBox.Show(Mensaje, Convert.ToString((!string.IsNullOrEmpty(Titulo) ? Titulo : "Mensaje del Sistema")), Botones, Icono);
        }

        public static void ValidaMdiParent()
        {
            //if (formPrinci.MdiChildren.Length == 0)
            //{
            //    formPrinci.IsMdiContainer = false;
            //}
            //formPrinci.IsMdiContainer = false;
            //foreach (Form form in Application.OpenForms)
            //{
            //    if (form.GetType() != typeof(FrmPrincipal))
            //    {
            //        formPrinci.IsMdiContainer = true;
            //        return;
            //    }
                
            //}
            for (int i = 0; i < Application.OpenForms.Count; i++)
            {
                Form form = Application.OpenForms[i];
                if (form.GetType() == typeof(FrmPrincipal) && i== Application.OpenForms.Count-1)
                {
                    formPrinci.IsMdiContainer = false;
                    return;
                }
            }
           
        }

        public static void AgregaServidorALista(string[] lista)
        {
            try
            {
                const string userRoot = "HKEY_CURRENT_USER";
                const string subkey = "GuiAcceso";
                const string keyName = userRoot + "\\" + subkey;
                Registry.SetValue(keyName, "Servidores", lista);              
            }
            catch (Exception ex)
            {
                msgError(ex.Message);
            }
        }

        public static string[] ObtenerListaDeServidores()
        {
            try
            {
                const string userRoot = "HKEY_CURRENT_USER";
                const string subkey = "GuiAcceso";
                const string keyName = userRoot + "\\" + subkey;
                string[] tArray = (string[])Registry.GetValue(keyName,"Servidores", new string[] { "192.168.1.1\\RAPIENTREGA" });
                listaServidores.Clear();
                for (int i = 0; i < tArray.Length; i++)
                {
                    listaServidores.Add(tArray[i]);
                    //Console.WriteLine("Servidores({0}): {1}", i, tArray[i]);
                }
                return tArray;
            }
            catch (Exception ex)
            {
                msgError(ex.Message);
                return null;
            }
        }

    }
}
